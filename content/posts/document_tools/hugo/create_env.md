+++
title = "Hugoの環境構築"
date = 2020-04-13T14:03:38Z
tags = ['hugo']
categories = ["document_tools"]
readingTime = true # show reading time after article date 
toc = true 
+++

良さげなdockerイメージがあったので使ってみた。

- https://hub.docker.com/r/jguyomard/hugo-builder

READMEに書いてあるとおりにすればめちゃかんたんにサーバー起動までできた。

しかしHugoのバージョンが少し古いので、イメージをカスタマイズして現時点での最新版を使えるようにする。

<!--more-->

```dockerfile
FROM alpine:latest

RUN apk add --no-cache \
    curl \
    git \
    openssh-client \
    rsync

ENV VERSION 0.69.0
RUN mkdir -p /usr/local/src \
    && cd /usr/local/src \

    && curl -L https://github.com/gohugoio/hugo/releases/download/v${VERSION}/hugo_${VERSION}_linux-64bit.tar.gz | tar -xz \
    && mv hugo /usr/local/bin/hugo \

    && curl -L https://bin.equinox.io/c/dhgbqpS8Bvy/minify-stable-linux-amd64.tgz | tar -xz \
    && mv minify /usr/local/bin/ \

    && addgroup -Sg 1000 hugo \
    && adduser -SG hugo -u 1000 -h /src hugo

WORKDIR /src

EXPOSE 1313
```

```shell
# docker build
docker build -t my/hugo-builder .

# プロジェクト作成
docker run --rm -it -v $PWD:/src -u hugo my/hugo-builder hugo new site mysite

# themeをあてる
cd mysite
git init
git submodule add https://github.com/cntrump/hugo-notepadium.git themes/hugo-notepadium
echo 'theme = "hugo-notepadium"' >> config.toml

# build
docker run --rm -it -v $PWD:/src -u hugo my/hugo-builder hugo

# server start
docker run --rm -it -v $PWD:/src -p 1313:1313 -u hugo my/hugo-builder hugo server -w --bind=0.0.0.0
```

buildとserver startは頻繁に使うのでaliasを作っとく

```shell
# vim ~/.config/fish/config.fish
alias hugo-build='docker run --rm -it -v $PWD:/src -u hugo my/hugo-builder hugo'
alias hugo-server='docker run --rm -it -v $PWD:/src -p 1313:1313 -u hugo my/hugo-builder hugo server -w --bind=0.0.0.0'
```
