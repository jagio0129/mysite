+++
title = "HugoをGitlab Pagesにデプロイ"
date = 2020-04-13T14:03:38Z
tags = ['hugo']
categories = ["document_tools"]
readingTime = true # show reading time after article date 
toc = true 
+++

[Hugoの環境構築](https://jagio0129.gitlab.io/mysite/posts/document_tools/hugo/create_env/)で作成したブログをGitlab pagesにデプロイしたいときに、サクッとできる方法。

<!--more-->

hugoのプロジェクトに`.gitlab-ci.yml`に作成して以下を記述。

```yaml
image: monachus/hugo

variables:
  GIT_SUBMODULE_STRATEGY: recursive

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - master
```

masterをpushもしくはmasterにマージされるたびにCIが走り、`https://<your_account>/<your_hugo_project>`でアクセスできるようになる。
