+++
title = "About me"
description = "My profile." 
+++

- [twitter](https://twitter.com/jagio0129)
- [Gitlab](https://gitlab.com/jagio0129)
- [Hatena Blog](http://jagio.hatenablog.com/)
- [読みたい技術本](https://www.amazon.jp/hz/wishlist/ls/2PYE6M2B34KXT?ref_=wl_share)
